package com.fineoz.fineoz_backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FineozBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(FineozBackendApplication.class, args);
	}

}
